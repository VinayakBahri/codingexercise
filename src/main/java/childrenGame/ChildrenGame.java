package childrenGame;

import java.util.ArrayList;

public class ChildrenGame {
	private ArrayList<Integer> idsOfKidsInGame; 
	
	public ChildrenGame(int KidsInGame) {
		idsOfKidsInGame = new ArrayList<Integer>();
		for (int i=0; i<KidsInGame;i++) {
			idsOfKidsInGame.add(i+1);
		}
	}

	public ArrayList<Integer> getList() {
		return idsOfKidsInGame;
	}
	
	public static int winnerOfTheGame(ArrayList<Integer> n,int k) {
		//to store the index at which the kid got out
		int index = 0;			
        while (n.size () > 1){  // O(number of kids in game-1)
            index = (index + k - 1) % n.size (); 
            System.out.println(n.get(index) + " is out");
            n.remove(index);           
        }
        System.out.println("And the winner is: " + n.get(0));
        return n.get(0);
	}
}
