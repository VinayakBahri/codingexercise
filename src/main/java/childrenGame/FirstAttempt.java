package childrenGame;

import java.util.ArrayList;
import java.util.Scanner;

public class FirstAttempt {
	
	private int numberOfChildren;
	private int  k;
	private ArrayList<Integer> idsOfChildren; 

	public static void main(String[] args) {		
		FirstAttempt fa = new FirstAttempt();
		fa.gameInitialize();
		fa.setChildIds();
		int winner = FirstAttempt.letsPlay(fa.idsOfChildren,fa.k);
		System.out.println("And the winner is: " + winner);
		}
	
	public void gameInitialize() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number of children playing:");
		numberOfChildren = sc.nextInt();
		System.out.println("Enter the magical number that will get children out: ");
		k = sc.nextInt();
		sc.nextLine();
		sc.close();
	}
	
	public void setChildIds() {
		idsOfChildren = new ArrayList<Integer>();
		for (int i=0; i<numberOfChildren;i++) {
			idsOfChildren.add(i+1);
		}
	}
	
	public static int letsPlay(ArrayList<Integer> n, int k2) {
		/*
		 * Need 2 variable 
		 * one(j) to traverse through the array 
		 * another(i) to keep track of magical number k
		 */
		int i = 0;
		int j= 0;
		while(n.size()>1) {
			boolean flag = true; //O(number of kid -1 * k)
			i++;
			if(i==k2) {
				System.out.println(n.get(j)+ " is out");  //O (k)
				n.remove(j);
				i=0;
				flag=false;
				//Need to reset the iterator j
				if(j==n.size()) {
					j=0;
					}
				}
			//Only reset/increment the iterator if it is not done above
			if(j==n.size()-1 && flag) {
				j=0;
				}else if(flag) {
					j++;
					}			
			}
		return n.get(0);
		
	}
}