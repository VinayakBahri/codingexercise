package childrenGame;
import static org.testng.Assert.assertEquals;

import java.io.ObjectInputStream.GetField;
import java.util.Arrays;
import java.util.List;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class ChildrenGameTest {
	
	@Test
	public void shouldReturnIdsOfChildren() {
		ChildrenGame c1 = new ChildrenGame(5);
		List<Integer> expectedIdsList = Arrays.asList( 1, 2, 3, 4, 5);
		assertEquals(c1.getList(),expectedIdsList );
	}
	
	@Test(dataProvider = "getData")
	public void shouldReturnTheWinner(int numberOfChildren, int magicalNumber, int Expected) {
		ChildrenGame c1 = new ChildrenGame(numberOfChildren);
		if(numberOfChildren>0 && magicalNumber >0 ) {
			assertEquals(ChildrenGame.winnerOfTheGame(c1.getList(), magicalNumber),Expected);			
		}else {
			System.out.println("number of children and magical number should be greater than 0 ");			
		}		
	}	
	
	@DataProvider
	public Object[][]getData(){
		Object data[][] = new Object[5][3];
		data[0][0]=5;
		data[0][1]=1;
		data[0][2]=5;
		
		data[1][0]=5;
		data[1][1]=5;
		data[1][2]=2;
		
		data[2][0]=5;
		data[2][1]=6;
		data[2][2]=4;
		
		data[3][0]=0;
		data[3][1]=6;
		data[3][2]=4;
		
		data[4][0]=1;
		data[4][1]=0;
		data[4][2]=4;		
		
		return data;
	}
}


